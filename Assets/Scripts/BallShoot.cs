using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour {
	
	public GameObject bullet_prefab;
	float bulletImpulse = 10f;

	
	// Update is called once per frame
	public void Shot () {
		GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
		thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
	}
}
