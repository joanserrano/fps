﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RobotGenerator : MonoBehaviour {

	public GameObject robot;
    public Transform[] points;
    public float timeToSpawn;
    

    IEnumerator Start()
    {

        while (true)
        {
            yield return new WaitForSeconds(timeToSpawn);

            GameObject irobot = Instantiate(robot);
            RobotBehaviour rb = irobot.GetComponent<RobotBehaviour>();
            rb.pathNodes = points;
        }
    }
}
